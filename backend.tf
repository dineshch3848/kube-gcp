terraform {
  backend "gcs" {
    bucket  = "bdb-tfstate"
    prefix  = "tf/state-02"
    credentials = "amuraa-bdb-sa-key.json"
    #credentials = "$BASE64_GOOGLE_CREDENTIALS"
  }
}
