resource "google_compute_forwarding_rule" "default" {
#  provider              = google-beta
  project               = "amuraa-bdb"
  name                  = "web-forwarding-rule"
  region                = "us-east1"
  port_range            = 80
  backend_service       = google_compute_region_backend_service.backend.id
#  network               = "us-east1-vpc-bdb"
#  subnetwork            = "us-east1-snet-pub-bdb"
}
resource "google_compute_region_backend_service" "backend" {
#  provider              = google-beta
  project               = "amuraa-bdb"
  name                  = "web-backend"
  region                = "us-east1"
  load_balancing_scheme = "EXTERNAL"
  health_checks         = [google_compute_region_health_check.hc.id]
}
resource "google_compute_region_health_check" "hc" {
#  provider           = google-beta
  project            = "amuraa-bdb"
  name               = "check-web-backend"
  check_interval_sec = 1
  timeout_sec        = 1
  region             = "us-east1"

  tcp_health_check {
    port = "80"
  }
}
