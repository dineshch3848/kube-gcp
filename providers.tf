terraform {
  required_version = ">= 0.12"
}
provider "google" {
  credentials = file("amuraa-bdb-sa-key.json")
  #credentials = "$BASE64_GOOGLE_CREDENTIALS"
  project     = "amuraa-bdb"
  region      = "us-east1"
}
