resource "google_compute_instance" "ygcpbdbmysql" {
  name = "ygcpbdbmysql"
  machine_type = "n2-standard-2"
  zone = "us-east1-b"

   tags = ["sql-network"]

  boot_disk {
    auto_delete  = false

    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      size = 50
      type = "pd-ssd"
    }
  }
   network_interface {
    subnetwork = "us-east1-snet-pvt-bdb"
  }

  // Override fields from instance template
  can_ip_forward = true
}

resource "google_compute_firewall" "sql-fw" {
  name    = "sql-fw"
  network = "us-east1-vpc-bdb"

  allow {
    protocol = "tcp"
    ports    = ["22", "3306"]
  }

  source_ranges = ["10.56.0.0/16"]
  target_tags   = ["sql-network"]
}
