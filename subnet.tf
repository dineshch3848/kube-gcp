module "vpc" {
    source  = "terraform-google-modules/network/google//modules/subnets"

    project_id   = "amuraa-bdb"
    network_name = "us-east1-vpc-bdb"

    subnets = [
        {
            subnet_name           = "us-east1-snet-pub-bdb"
            subnet_ip             = "10.56.10.128/28"
            subnet_region         = "us-east1"
        },
    ]
}

